
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LucianoTadeu
 */
public class Pratica31 {
    private static String meuNome = "Luciano Tadeu Esteves Pansanato";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1968, Calendar.APRIL, 28);
    
    public void programaPrincipal() {
            Date inicio = new Date();
            System.out.println(Pratica31.meuNome.toUpperCase());
            System.out.println(meuNome.substring(22,23).toUpperCase()+
                    meuNome.substring(23).toLowerCase()+", "+
                    meuNome.substring(0,1).toUpperCase()+". "+
                    meuNome.substring(8,9).toUpperCase()+". "+
                    meuNome.substring(14,15).toUpperCase()+".");
        
            GregorianCalendar dataAtual = new GregorianCalendar();
            long ms = dataAtual.getTime().getTime() - dataNascimento.getTime().getTime();
            System.out.println(ms/(24*60*60*1000));
            Date fim = new Date();
            System.out.println(fim.getTime() - inicio.getTime());
    }
}
